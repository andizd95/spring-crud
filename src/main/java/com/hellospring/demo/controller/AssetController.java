package com.hellospring.demo.controller;

import com.hellospring.demo.exception.ResourceNotFoundException;
import com.hellospring.demo.model.Asset;
import com.hellospring.demo.repository.AssetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@RequestMapping("api/v1")
public class AssetController {

    @Autowired
    private AssetRepository assetRepository;

    @GetMapping("/asset")
    public Page<Asset> getPageAssetSort(@RequestParam(defaultValue = "0") Integer page,
                                           @RequestParam(defaultValue = "10")Integer size,
                                           @RequestParam(defaultValue = "id") String sortBy){
        Pageable paging = PageRequest.of(page,size, Sort.by(sortBy).ascending());

        return assetRepository.findAll(paging);
    }

    @GetMapping("/asset/search/{name}")
    public ResponseEntity<Object> getAssetName(@PathVariable String name) {
        Optional<Asset> asset_detail = Optional.ofNullable(assetRepository.findByName(name))
                .orElseThrow(() -> new ResourceNotFoundException("Asset Null"));
        return new ResponseEntity<>(asset_detail, HttpStatus.OK);
    }

    @GetMapping("/asset/prices/{price}")
    public ResponseEntity<Object> getAssetPrice(@PathVariable int price) {
        Optional<Asset> asset = Optional.ofNullable(assetRepository.findByPrice(price))
                .orElseThrow(() -> new ResourceNotFoundException("Asset Null"));
        return new ResponseEntity<>(asset, HttpStatus.OK);
    }


    @GetMapping("/asset/{id}")
    public ResponseEntity<Object> getAssetByID(@PathVariable Long id){
        Asset asset = assetRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("item not found"));
        return new ResponseEntity<>(asset, HttpStatus.OK);
    }

    @PostMapping("/asset")
    public ResponseEntity<Object> createAsset(@RequestBody Asset asset) {
        return new ResponseEntity<>(assetRepository.save(asset), HttpStatus.CREATED);
    }

    @PutMapping("/asset/{id}")
    public ResponseEntity<Object> updateAsset(@PathVariable Long id, @RequestBody Asset asset){
        Asset asset_detail = assetRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("item not found"));

        asset_detail.setName(asset.getName());
        asset_detail.setQty(asset.getQty());
        asset_detail.setPrice(asset.getPrice());
        assetRepository.save(asset_detail);
        return new ResponseEntity<>(asset_detail,HttpStatus.OK);
    }

    @DeleteMapping("/asset/{id}")
    public ResponseEntity<Object> deleteAsset(@PathVariable Long id) {
        Asset asset_detail = assetRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("item not found"));

        assetRepository.delete(asset_detail);
        return new ResponseEntity<>(asset_detail, HttpStatus.OK);
    }

    //    @GetMapping("/asset")
//    public Page<Asset> getPageAsset(Pageable page){
//
//        return assetRepository.findAll(page);
//    }

//    @GetMapping("/asset")
//    public ResponseEntity<Object> getAsset(){
//        List<Asset> asset=assetRepository.findAll();
//        return new ResponseEntity<>(asset, HttpStatus.OK);
//    }
    //Pencarian data barang by name
//    @GetMapping("/asset/name")
//    public ResponseEntity<Object> getItemByName(@RequestParam String name) {
//        List<Asset> items = Optional.ofNullable(assetRepository.findByName(name)).orElseThrow(() ->
//                new ResourceNotFoundException("item not found"));
//        return new ResponseEntity<>(items, HttpStatus.OK);
//    }


    //Pencarian data barang by price
//    @GetMapping("/asset/price")
//    public ResponseEntity<Object> getAssetPrice(@RequestParam Integer price){
//        Optional<Asset> assets = Optional.ofNullable(assetRepository.findByPrice(price))
//                .orElseThrow(() -> new ResourceNotFoundException("Asset Not Found"));
//
//        return new ResponseEntity<>(assets, HttpStatus.OK);
//
//
//    }

    //Pencarian data barang by qty
//    @GetMapping("/asset/qty")
//    public ResponseEntity<Object> getAssetQty(@RequestParam Integer qty){
//        Optional<Asset> assets = Optional.ofNullable(assetRepository.findByQty(qty))
//                .orElseThrow(() -> new ResourceNotFoundException("Asset Not Found"));
//
//        return new ResponseEntity<>(assets, HttpStatus.OK);
//
//
//    }
}
