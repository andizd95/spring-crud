package com.hellospring.demo.repository;

import com.hellospring.demo.model.Asset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface AssetRepository extends JpaRepository<Asset,Long> {

//    List<Asset> findByName(String names);

    Optional<Asset> findByName(String name);
    Optional<Asset> findByPrice(int price);

//    Optional<Asset> findByQty(Integer qty);

}
